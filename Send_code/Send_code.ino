#include <IRremote.h>
#include <IRremoteInt.h>
 
IRsend irsend;
int ledStateOn=6;
int ledStateOff=5;
int pinButton_hdmi = 2;
int pinButton_pwr = 4;
int pinButton_console=8;
int valor_button_hdmi; 
int valor_button_pwr;
int valor_button_console; 
int outputPinConsole=10;
long contador_cycles=0;
int contador_time_sec=0;
int contador_time_minute=0;
int contador_time_hours=0;
int pressBtnPwr=0;
bool stateOn=false;
bool stateOnConsole=false;
int cntPwr=3;
 
void setup() {
  Serial.begin(9600);
  pinMode(ledStateOn,OUTPUT);
  pinMode(ledStateOff,OUTPUT);
  pinMode(outputPinConsole,OUTPUT);
  //setup out pins 
  digitalWrite(ledStateOn,LOW);
  digitalWrite(ledStateOff,LOW);
  digitalWrite(outputPinConsole,HIGH);
  //setup in pins 
  pinMode(pinButton_hdmi,INPUT_PULLUP);
  pinMode(pinButton_pwr,INPUT_PULLUP);
  pinMode(pinButton_console,INPUT_PULLUP);
  
}
 
void loop() {
  valor_button_hdmi = digitalRead(pinButton_hdmi);
  valor_button_pwr=digitalRead(pinButton_pwr);
  valor_button_console=digitalRead(pinButton_console);
  
  if(valor_button_pwr == LOW)
  {
    if((stateOn==false)&&(stateOnConsole==false))
    {
            digitalWrite(outputPinConsole,LOW);
            stateOnConsole=!stateOnConsole;
            
      }
    send_pwr(cntPwr);    
    stateOn=!stateOn;

    
  }  
  if(valor_button_hdmi == LOW)
  {
    sendHDMI(1);
  }
  if(valor_button_console==LOW)
  {

          if((stateOn==false)&&(stateOnConsole==false))
      {
          send_pwr(cntPwr);
    
          stateOn=!stateOn;
        }        
    stateOnConsole=!stateOnConsole;
    if(stateOnConsole==true)
    {
      digitalWrite(outputPinConsole,LOW);
      }
        if(stateOnConsole==false)
    {
      digitalWrite(outputPinConsole,HIGH);

    }

        delay(500);    

    }
      if((stateOn==true)||(stateOnConsole==true))
    {
      time_minute();
      digitalWrite(ledStateOn,HIGH);
      digitalWrite(ledStateOff,LOW);
    }
    if(stateOn==false)
    {
      digitalWrite(ledStateOn,LOW);
      digitalWrite(ledStateOff,HIGH);
      
      }
  if((stateOn==false)&&(stateOnConsole==false))
  {
    resetTime();
    }
}
void resetTime()
{
     contador_cycles=0;
     contador_time_sec=0;
     contador_time_minute=0;
     contador_time_hours=0;  
  }

void sendHDMI(int cnt)
{
  for (int i=0; i<cnt;i++)
  {
    irsend.sendNEC(0x20DFD02F,32);
    Serial.println("Enviado");
    delay(500);    
    }
  }
 
void send_pwr(int cnt)
{
  for(int i=0; i<cnt; i++)
  {
    irsend.sendNEC(0x20DF10EF,32);
    Serial.println("Enviado");
    delay(500);  
  }
  }
void time_minute()
{
  contador_cycles++;
  if(contador_cycles==37000)
  {Serial.print("Segundo: ");
  contador_cycles=0;
  contador_time_sec++;
  Serial.println(contador_time_sec,DEC);
  if(contador_time_sec==60)
  {

    contador_time_minute++;
    contador_time_sec=0;
    Serial.print("------------- minuto-------------");
    Serial.println(contador_time_minute,DEC);
    
    }
    if(contador_time_minute==60)
    {
      contador_time_hours++;
      contador_time_minute=0;
      Serial.println("------------- hora-------------");
      Serial.println(contador_time_hours,DEC);
      if (contador_time_hours==2)
      {
          if(stateOn==true)    
            {
              send_pwr(cntPwr);
              stateOn=false;
            }
            if(stateOnConsole==true)
            {
              stateOnConsole=false;
              digitalWrite(outputPinConsole,HIGH);
            }
            contador_time_hours=0;
            
        }
      }
  }
  }
